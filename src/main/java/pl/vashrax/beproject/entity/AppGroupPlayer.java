package pl.vashrax.beproject.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "app_group_player")
public class AppGroupPlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long appGroupPlayerId;
    private long appGroupId;
    private long appUserId;
}

package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.Chat;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
    Optional<Chat> findFirstByAppUserIdInAndAppAppUserIdIn(long[] appUserIds, long[] appUserIdss);
    Optional<List<Chat>> findAllByAppAppUserIdOrAppUserId(long appUserId, long appUserIdd);
    boolean existsByChatId(long chatId);

}

package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.entity.AppGroup;
import pl.vashrax.beproject.entity.AppGroupChatMessage;
import pl.vashrax.beproject.entity.AppGroupPlayer;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.services.AppGroupService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
@RequestMapping("/groups")
public class AppGroupController {

    @Autowired
    private AppGroupService appGroupService;



    @GetMapping("/{appGroupId}")
    public ResponseEntity<AppGroup> getAppGroupByAppGroupId(
            @PathVariable("appGroupId") long appGroupId){
        AppGroup appGroup = appGroupService.getAppGroupById(appGroupId);
        return new ResponseEntity<>(appGroup, HttpStatus.OK);
    }
    @PostMapping("/users/{appUserId}")
    public ResponseEntity<List<AppGroup>> getAppGroupsForAppUserId(@PathVariable(
            "appUserId") long appUserId, @RequestBody long gameId){
        List<AppGroup> appGroups = appGroupService.getAppGroupsForAppUserId(appUserId, gameId);
        return new ResponseEntity<>(appGroups, HttpStatus.OK);
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<List<AppGroup>> getAppGroupsForGameId(@PathVariable(
            "gameId") long gameId){
        List<AppGroup> appGroups = appGroupService.getAppGroupsForGameId(gameId);
        return new ResponseEntity<>(appGroups, HttpStatus.OK);
    }

    @PostMapping("/users/{appUserId}/noanno")
    public ResponseEntity<List<AppGroup>> getAppGroupsForAppUserForAnnouncements(@PathVariable(
            "appUserId") long appUserId, @RequestBody long gameId){
        List<AppGroup> appGroups = appGroupService.getAppGroupsForAppUserIdForAnnouncements(appUserId, gameId);
        return new ResponseEntity<>(appGroups, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<AppGroup> createAppGroup(@RequestBody AppGroup appGroupToCreate) {
        AppGroup appGroup = appGroupService.createAppGroup(appGroupToCreate);
        return new ResponseEntity<>(appGroup, OK);
    }

    @PutMapping("")
    public ResponseEntity<AppGroup> updateAppGroup(@RequestBody AppGroup appGroupToCreate) {
        AppGroup appGroup = appGroupService.updateAppGroup(appGroupToCreate);
        return new ResponseEntity<>(appGroup, OK);
    }

    @DeleteMapping("/{appGroupId}")
    public ResponseEntity<?> deleteAppGroup(@PathVariable(
            "appGroupId") long appGroupId) {
        appGroupService.deleteAppGroup(appGroupId);
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/{appGroupId}/users")
    public ResponseEntity<List<AppUser>>  getAppUsersForAppGroup(
            @PathVariable("appGroupId") long appGroupId){
        List<AppUser> appUsers = appGroupService.getAppUsersForAppGroup(appGroupId);
        return new ResponseEntity<>(appUsers, HttpStatus.OK);
    }
    @PostMapping("/{appGroupId}/users")
    public ResponseEntity<AppGroupPlayer> addAppUserToAppGroup(
            @PathVariable("appGroupId") long appGroupId, @RequestBody long appUserId) {
        AppGroupPlayer appGroupPlayer = appGroupService.addAppUserToAppGroup(appUserId, appGroupId);
        return new ResponseEntity<>(appGroupPlayer, OK);
    }

    @DeleteMapping("/{appGroupId}/users")
    public ResponseEntity<?> removeAppUserFromAppGroup(
            @PathVariable("appGroupId") long appGroupId, @RequestBody long appUserId) {
        appGroupService.removeAppUserFromAppGroup(appUserId, appGroupId);
        return new ResponseEntity<>(OK);
    }

    @GetMapping("/{appGroupId}/messages")
    public ResponseEntity<List<AppGroupChatMessage>> getAppGroupChatMessages(
            @PathVariable("appGroupId") long appGroupId){
        List<AppGroupChatMessage> appGroupChatMessages = appGroupService.getAppGroupChatMessages(appGroupId);
        return new ResponseEntity<>(appGroupChatMessages, HttpStatus.OK);
    }

    @PostMapping("/{appGroupId}/messages")
    public ResponseEntity<AppGroupChatMessage> addAppGroupChatMessageToAppGroup(
            @PathVariable("appGroupId") long appGroupId, @RequestBody AppGroupChatMessage appGroupChatMessageToSave){
        AppGroupChatMessage appGroupChatMessage = appGroupService.addAppGroupChatMessageToAppGroup(appGroupChatMessageToSave, appGroupId);
        return new ResponseEntity<>(appGroupChatMessage, HttpStatus.OK);
    }

}

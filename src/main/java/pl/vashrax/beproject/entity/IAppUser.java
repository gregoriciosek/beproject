package pl.vashrax.beproject.entity;

import java.util.Date;

public interface IAppUser {

    Long getAppUserId();

    void setAppUserId(Long appUserId);

    Long getAppUserTypeId();

    void setAppUserTypeId(Long appUserTypeId);

    String getAppUserUsername();

    void setAppUserUsername(String appUserUsername);

    String getAppUserPassword();

    void setAppUserPassword(String appUserPassword);

    String getAppUserEmail();

    void setAppUserEmail(String appUserEmail);

    String getAppUserNickname();

    void setAppUserNickname(String appUserNickname);

    Date getAppUserDateOfBirth();

    void setAppUserDateOfBirth(Date appUserDateOfBirth);

    Date getCreatedAt();

    void setCreatedAt(Date createdAt);

    String getAppUserDiscord();

    void setAppUserDiscord(String appUserDiscord);

    String getAppUserTeamspeak();

    void setAppUserTeamspeak(String appUserTeamspeak);

    String getAppUserGender();

    void setAppUserGender(String appUserGender);

    String getAppUserProfileImgUrl();

    void setAppUserProfileImgUrl(String appUserProfileImgUrl);



}

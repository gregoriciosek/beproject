package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.dto.AppUserDto;
import pl.vashrax.beproject.exception.domain.CredencialsIncorrectException;
import pl.vashrax.beproject.mapper.AppUserMapper;
import pl.vashrax.beproject.services.AppUserService;
import pl.vashrax.beproject.security.UserPrincipal;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.security.JwtTokenProvider;

import static pl.vashrax.beproject.config.SecurityConstant.*;

@Controller
@RequestMapping("/")
@CrossOrigin("http://localhost:4200")
@AllArgsConstructor
public class LoginController {

    private final AppUserService appUserService;
    private final DaoAuthenticationProvider daoAuthenticationProvider;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public ResponseEntity<AppUserDto> login(@RequestBody AppUser appUser) throws CredencialsIncorrectException {
        try {
            UserPrincipal loginUser = (UserPrincipal) appUserService.loadUserByUsername(appUser.getAppUserUsername());
            authenticate(appUser.getAppUserUsername(), appUser.getAppUserPassword());
            HttpHeaders jwtHeader = getJwtHeader(loginUser);
            return ResponseEntity.ok().headers(jwtHeader).body(AppUserMapper.mapToDto(appUserService.loadApplicationUserByUsername(appUser.getAppUserUsername())));
        } catch (UsernameNotFoundException ex) {
            throw new CredencialsIncorrectException();
        }
    }

    private HttpHeaders getJwtHeader(UserPrincipal loginUser) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER, TOKEN_PREFIX + jwtTokenProvider.generateJwtToken(loginUser));
        return headers;
    }

    private void authenticate(String username, String password) {
        daoAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(username,password));
    }

}

package pl.vashrax.beproject.security;

import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.enums.AppUserRole;

import java.util.Collection;

@AllArgsConstructor
public class UserPrincipal implements UserDetails {

    private AppUser appUser;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AppUserRole.parseId(this.appUser.getAppUserTypeId()).getGrantedAuthorities();
    }

    @Override
    public String getPassword() {
        return this.appUser.getAppUserPassword();
    }

    @Override
    public String getUsername() {
        return this.appUser.getAppUserUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

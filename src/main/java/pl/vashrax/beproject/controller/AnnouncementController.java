package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.entity.Announcement;
import pl.vashrax.beproject.entity.AppGroupPlayer;
import pl.vashrax.beproject.services.AnnouncementService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
@RequestMapping("/announcements")
public class AnnouncementController {

    @Autowired
    private AnnouncementService announcementService;

    @GetMapping("/games/{gameId}")
    public ResponseEntity<List<Announcement>> getAnnouncementsByGameId(@PathVariable(
            "gameId") long gameId){
        List<Announcement> announcements = announcementService.getAnnouncementsForGameId(gameId);
        return new ResponseEntity<>(announcements, HttpStatus.OK);
    }

    @GetMapping("/{announcementId}")
    public ResponseEntity<Announcement> getAnnouncementsByAnnouncementId(
            @PathVariable("announcementId") long announcementId){
        Announcement announcement = announcementService.getAnnouncementByAnnouncementId(announcementId);
        return new ResponseEntity<>(announcement, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Announcement> createAnnouncement(@RequestBody Announcement announcement) {
        Announcement newAnnouncement = announcementService.createAnnouncement(announcement);
        return new ResponseEntity<>(newAnnouncement, OK);
    }

    @PutMapping("")
    public ResponseEntity<Announcement> updateAnnouncement(@RequestBody Announcement announcement) {
        Announcement newAnnouncement = announcementService.updateAnnouncement(announcement);
        return new ResponseEntity<>(newAnnouncement, OK);
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteAnnouncement(@RequestBody Announcement announcement) {
        announcementService.deleteAnnouncement(announcement);
        return new ResponseEntity<>(OK);
    }

    @PostMapping("/groups/users")
    public ResponseEntity<AppGroupPlayer> addAppUserToGroupByAnnouncement(@RequestBody AppGroupPlayer appGroupPlayerToSave) {
        AppGroupPlayer appGroupPlayer = announcementService.addAppUserToGroupByAnnouncement(appGroupPlayerToSave);
        return new ResponseEntity<>(appGroupPlayer, OK);
    }





}

package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.dto.AppUserFilterDto;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.services.AppUserService;

import java.util.List;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class AppUserController {

    private final AppUserService appUserService;

    @GetMapping("")
    public ResponseEntity<List<AppUser>> getAllUsers(){
        List<AppUser> users = appUserService.findAllApplicationUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/{appUserId}")
    public ResponseEntity<AppUser> getAllUsers(@PathVariable("appUserId") Integer appUserId){
        AppUser user = appUserService.findById(appUserId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/find/{username}")
    public ResponseEntity<AppUser> getUserByUsername(@PathVariable("username") String username){
        AppUser user = appUserService.loadApplicationUserByUsername(username);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<AppUser> addUser(@RequestBody AppUser user){
        AppUser userToSave = new AppUser();
        userToSave.setAppUserUsername(user.getAppUserUsername());
        userToSave.setAppUserPassword(user.getAppUserPassword());
        userToSave.setAppUserNickname(user.getAppUserNickname());
        userToSave.setAppUserTypeId(user.getAppUserTypeId());
        AppUser newUser = appUserService.addAndReturnNewUser(userToSave);
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    @PostMapping("/filtered")
    public ResponseEntity<List<AppUser>> getAppUsersByFilter(@RequestBody AppUserFilterDto appUserFilter){
        List<AppUser> appUsers = appUserService.getAppUsersByFilter(appUserFilter);
        return new ResponseEntity<>(appUsers, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<AppUser> updateUser(@RequestBody AppUser user){
        AppUser newUser = new AppUser();
        newUser.setAppUserUsername(user.getAppUserUsername());
        newUser.setAppUserPassword(user.getAppUserPassword());
        newUser.setAppUserNickname(user.getAppUserNickname());
        newUser.setAppUserTypeId(user.getAppUserTypeId());
        newUser.setAppUserProfileImgUrl(user.getAppUserProfileImgUrl());
        newUser.setAppUserEmail(user.getAppUserEmail());
        newUser.setAppUserDateOfBirth(user.getAppUserDateOfBirth());
        newUser.setAppUserGender(user.getAppUserGender());
        newUser.setAppUserDiscord(user.getAppUserDiscord());
        newUser.setAppUserTeamspeak(user.getAppUserTeamspeak());
        AppUser updateUser = appUserService.updateAndReturnUser(newUser);
        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }

    @DeleteMapping("/{appUserId}")
    public ResponseEntity<?> deleteUser(@PathVariable("appUserId")Long appUserId){
        appUserService.deleteById(appUserId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

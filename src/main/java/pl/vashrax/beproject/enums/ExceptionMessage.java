package pl.vashrax.beproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ExceptionMessage {

    CREDEMCIALS_INCORRECT_EXCEPTION("Nieprawidłowe dane logowania!"),
    USERNAME_NOT_FOUND_EXCEPTION("Nie znaleziono użytkownika o podanym loginie!"),
    HTTP_METHOD_NOT_SUPPORTED_EXCEPTION("Metoda '%s' jest niedozwolona w tym miejscu!");

    @Getter
    private String message;

    public ExceptionMessage parse(String message) {
        for (ExceptionMessage exceptionMessage : values()) {
            if (exceptionMessage.getMessage().equals(message)) {
                return exceptionMessage;
            }
        }
        return null;
    }

}

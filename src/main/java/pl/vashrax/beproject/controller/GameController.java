package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.entity.AppUserGame;
import pl.vashrax.beproject.entity.Attribute;
import pl.vashrax.beproject.entity.Game;
import pl.vashrax.beproject.services.AppUserGameService;
import pl.vashrax.beproject.services.AttributeService;
import pl.vashrax.beproject.services.GameService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@Controller
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
@RequestMapping("/games")
public class GameController {

    @Autowired
    private final GameService gameService;

    @Autowired
    private final AttributeService attributeService;

    @Autowired
    private final AppUserGameService appUserGameService;

    @GetMapping("")
    public ResponseEntity<List<Game>> getAllGames(){
        List<Game> games = gameService.findAllGames();
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @GetMapping("/{gameId}/attributes")
    public ResponseEntity<List<Attribute>> getAllUsers(@PathVariable("gameId") long gameId){
        List<Attribute> attributes = attributeService.getAttributesByGameId(gameId);
        return new ResponseEntity<>(attributes, HttpStatus.OK);
    }

    @PutMapping("/attributes")
    public ResponseEntity<AppUserGame> addOrUpdateAttributeForGameAndAppUser(@RequestBody AppUserGame appUserGame) {
        return new ResponseEntity<>(appUserGameService.addOrUpdateAttributeForGameAndAppUser(appUserGame), OK);
    }

    @PostMapping("/attributes")
    public ResponseEntity<AppUserGame> getAttributeForGameAndAppUser(@RequestBody AppUserGame appUserGame) {
        return new ResponseEntity<>(appUserGameService.getAttributeForGameAndAppUser(appUserGame), OK);
    }

}

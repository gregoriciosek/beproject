package pl.vashrax.beproject.enums;


import com.google.common.collect.Sets;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static pl.vashrax.beproject.enums.AppUserPermission.*;

public enum AppUserRole {
    USER(1L, Sets.newHashSet(USER_AUTH)),
    ADMIN(2L, Sets.newHashSet(ADMIN_AUTH));

    @Getter
    private Long id;
    @Getter
    private Set<AppUserPermission> permissions;

    AppUserRole(Long id, Set<AppUserPermission> permissions) {
        this.id = id;
        this.permissions = permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities(){
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }

    public static AppUserRole parseId(Long id) {
        for (AppUserRole role : AppUserRole.values()) {
            if (role.getId() == id) {
                return role;
            }
        }
        return null;
    }
}

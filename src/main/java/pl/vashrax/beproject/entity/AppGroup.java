package pl.vashrax.beproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "app_group")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AppGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long appGroupId;
    private String appGroupName;
    private String appGroupDescription;
    private Long gameId;
    private Long appUserId;

}

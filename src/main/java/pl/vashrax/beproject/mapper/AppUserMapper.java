package pl.vashrax.beproject.mapper;

import pl.vashrax.beproject.dto.AppUserDto;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.entity.IAppUser;

public class AppUserMapper {

    public static AppUser mapToEntity(AppUserDto appUserDto) {
        return (AppUser) map(appUserDto, new AppUser());
    }

    public static AppUserDto mapToDto(AppUser appUser) {
        return (AppUserDto) map(appUser, new AppUserDto());
    }

    private static IAppUser map(IAppUser sourceAppUser, IAppUser targetAppUser) {

        IAppUser iAppUser = targetAppUser;

        iAppUser.setAppUserId(sourceAppUser.getAppUserId());
        iAppUser.setAppUserTypeId(sourceAppUser.getAppUserTypeId());
        iAppUser.setAppUserUsername(sourceAppUser.getAppUserUsername());
        iAppUser.setAppUserPassword(sourceAppUser.getAppUserPassword());
        iAppUser.setAppUserEmail(sourceAppUser.getAppUserEmail());
        iAppUser.setAppUserNickname(sourceAppUser.getAppUserNickname());
        iAppUser.setAppUserDateOfBirth(sourceAppUser.getAppUserDateOfBirth());
        iAppUser.setCreatedAt(sourceAppUser.getCreatedAt());
        iAppUser.setAppUserDiscord(sourceAppUser.getAppUserDiscord());
        iAppUser.setAppUserTeamspeak(sourceAppUser.getAppUserTeamspeak());
        iAppUser.setAppUserGender(sourceAppUser.getAppUserGender());
        iAppUser.setAppUserProfileImgUrl(sourceAppUser.getAppUserProfileImgUrl());

        return iAppUser;
    }

}

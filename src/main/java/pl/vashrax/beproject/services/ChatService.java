package pl.vashrax.beproject.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.vashrax.beproject.dto.ChatUsersAndChatMessageDto;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.entity.Chat;
import pl.vashrax.beproject.entity.ChatMessage;
import pl.vashrax.beproject.repository.ChatMessageRepository;
import pl.vashrax.beproject.repository.ChatRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class ChatService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final ChatRepository chatRepository;

    private final AppUserService appUserService;

    private final ChatMessageRepository chatMessageRepository;

    public List<Chat> findAllChats(){
        return chatRepository.findAll();
    }

    public Chat findChatbyId(long id) {
        Chat chat = chatRepository.findById(id).orElseThrow(
                () -> new IllegalStateException("Nie ma czatu z id "+ id + ".")
        );
        return chat;
    }

    @Transactional
    public Chat findChatByAppUserIds(long[] appUserIds) {
        Optional<Chat> chat = chatRepository.findFirstByAppUserIdInAndAppAppUserIdIn(appUserIds, appUserIds);
        if (chat.isPresent()) {
            return chat.get();
        } else {
            Chat newChat = new Chat();
            newChat.setAppUserId(appUserIds[0]);
            newChat.setAppAppUserId(appUserIds[1]);
            return chatRepository.save(newChat);
        }
    }

    public List<Chat> findChatsByAppUserId(long appUserId) {
        Optional<List<Chat>> chats = chatRepository.findAllByAppAppUserIdOrAppUserId(appUserId,appUserId);
        if (chats.isPresent()) {
            return chats.get();
        }
        return null;
    }

    public List<AppUser> findAppUsersByChatsByAppUserId(long appUserId) {
        ArrayList<AppUser> users = new ArrayList<>();
        Optional<List<Chat>> chats = chatRepository.findAllByAppAppUserIdOrAppUserId(appUserId,appUserId);
        if (chats.isPresent()) {
            for(Chat chat : chats.get()) {
                if (chat.getAppUserId() != appUserId) {
                    users.add(appUserService.findById(chat.getAppUserId()));
                } else {
                    users.add(appUserService.findById(chat.getAppAppUserId()));
                }
            }
        }
        return users;
    }

    public List<ChatMessage> getChatMessagesByChatId(long chatId) {
        Optional<List<ChatMessage>> messages = chatMessageRepository.findAllByChatId(chatId);
        if (messages.isPresent()) {
            return messages.get();
        } else {
            return null;
        }
    }

    @Transactional
    public ChatMessage createMessageForSenderIdAndChatId(long senderId, long chatId, String message) {
        ChatMessage messageToSave = new ChatMessage();
        messageToSave.setChatId(chatId);
        messageToSave.setAppUserId(senderId);
        messageToSave.setChatMessageMsg(message);
        messageToSave.setChatMessageDate(new Date());
        return chatMessageRepository.save(messageToSave);
    }

    public boolean existsByChatId(long chatId) {
        return chatRepository.existsByChatId(chatId);
    }

    public Chat createChat(Chat chat) {
        return chatRepository.save(chat);
    }

}

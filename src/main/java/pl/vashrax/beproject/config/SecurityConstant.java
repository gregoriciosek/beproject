package pl.vashrax.beproject.config;

public class SecurityConstant {
    public static final long EXPIRATION_TIME = 432_000_000; //5 DAYS
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String TOKEN_CANNOT_BE_VERIFIED ="Token cannot be verified";
    public static final String PL_VASHRAX = "Vashrax";
    public static final String PL_VASHRAX_ADMINISTRATION = "User Management";
    public static final String AUTHORITIES = "authorities";
    public static final String FORBIDDEN_MESSAGE = "You need login to see this page.";
    public static final String ACCESS_DENIED_MESSAGE = "No permissions to access this resource.";
    public static final String OPTION_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = {"/login", "/register"};
}

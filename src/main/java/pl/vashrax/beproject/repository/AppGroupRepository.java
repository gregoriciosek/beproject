package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.AppGroup;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface AppGroupRepository extends JpaRepository<AppGroup, Long> {

    Optional<List<AppGroup>> findAllByGameId(long gameId);

}


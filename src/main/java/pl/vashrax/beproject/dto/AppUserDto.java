package pl.vashrax.beproject.dto;

import lombok.Data;
import pl.vashrax.beproject.entity.IAppUser;

import java.util.Date;

@Data
public class AppUserDto implements IAppUser {

    private Long appUserId;
    private Long appUserTypeId;
    private String appUserUsername;
    private String appUserPassword;
    private String appUserEmail;
    private String appUserNickname;
    private Date appUserDateOfBirth;
    private Date createdAt;
    private String appUserDiscord;
    private String appUserTeamspeak;
    private String appUserGender;
    private String appUserProfileImgUrl;

}

package pl.vashrax.beproject.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "groupchatmessage")
public class GroupChatMessage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date sendTime;
    private String text;

    private Date getDateBack(int daysBack, int hoursBack){
        long timeToBack = countTime(daysBack, hoursBack);
        Date date = new Date(new Date().getTime() - timeToBack);
        return date;
    }

    private long countTime(int days, int hours) {
        return (hours * 3_600_000 + days * 86_400_000);
    }
}

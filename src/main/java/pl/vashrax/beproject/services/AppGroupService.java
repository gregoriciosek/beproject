
package pl.vashrax.beproject.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.vashrax.beproject.entity.*;
import pl.vashrax.beproject.repository.*;
import pl.vashrax.beproject.utils.DataValidationUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AppGroupService {

    private final AppGroupRepository appGroupRepository;

    private final AppGroupChatRepository appGroupChatRepository;

    private final AppGroupChatMessageRepository appGroupChatMessageRepository;

    private final AppGroupPlayerRepository appGroupPlayerRepository;

    private final AppUserRepository appUserRepository;

    private final AnnouncementRepository announcementRepository;

    public List<AppUser> getAppUsersForAppGroup(long appGroupId) {
        Optional<List<AppGroupPlayer>> appGroupsForPlayer = appGroupPlayerRepository.findAllByAppGroupId(appGroupId);
        if(appGroupsForPlayer.isPresent()) {
            List<AppUser> appUsersForAppGroup = new ArrayList<>();
            for(AppGroupPlayer appGroupPlayer : appGroupsForPlayer.get()) {
                appUsersForAppGroup.add(appUserRepository.getById(appGroupPlayer.getAppUserId()));
            }
            return appUsersForAppGroup;
        }
        return null;
    }

    public AppGroup getAppGroupById(long appGroupId) {
        return appGroupRepository.getById(appGroupId);
    }

    public List<AppGroup> getAppGroupsForAppUserId(long appUserId, long gameId) {
        Optional<List<AppGroupPlayer>> appGroupsForPlayer = appGroupPlayerRepository.findAllByAppUserId(appUserId);
        if(appGroupsForPlayer.isPresent()) {
            List<AppGroup> groupsForPlayer = new ArrayList<>();
            for(AppGroupPlayer appGroupPlayer : appGroupsForPlayer.get()) {
                AppGroup appGroup = (appGroupRepository.getById(appGroupPlayer.getAppGroupId()));
                if (appGroup.getGameId() == gameId)
                    groupsForPlayer.add(appGroup);
            }
            return groupsForPlayer;
        }
        return null;
    }

    public List<AppGroup> getAppGroupsForGameId(long gameId) {
        Optional<List<AppGroup>> appGroups = appGroupRepository.findAllByGameId(gameId);
        if (appGroups.isPresent()) {
            return appGroups.get();
        }
        return null;
    }

    public List<AppGroup> getAppGroupsForAppUserIdForAnnouncements(long appUserId, long gameId) {
        Optional<List<AppGroupPlayer>> appGroupsForPlayer = appGroupPlayerRepository.findAllByAppUserId(appUserId);
        if(appGroupsForPlayer.isPresent()) {
            List<AppGroup> groupsForPlayer = new ArrayList<>();
            for(AppGroupPlayer appGroupPlayer : appGroupsForPlayer.get()) {
                AppGroup appGroup = (appGroupRepository.getById(appGroupPlayer.getAppGroupId()));
                if (appGroup.getGameId() == gameId) {
                    Optional<List<Announcement>> announcements = announcementRepository.findAllByGameIdAndAppUserIdAndAppGroupId(gameId, appUserId, appGroup.getAppGroupId());
                    if (announcements.get().size() == 0) {
                        groupsForPlayer.add(appGroup);
                    }
                }
            }
            if (groupsForPlayer.isEmpty()) {
                return null;
            }
            return groupsForPlayer;
        }
        return null;
    }

    public List<AppGroupChatMessage> getAppGroupChatMessages(long appGroupId) {
        AppGroupChat appGroupChat = appGroupChatRepository.findFirstByAppGroupId(appGroupId).get();

        Optional<List<AppGroupChatMessage>> appGroupChatMessagesOptional =
                appGroupChatMessageRepository.findAllByAppGroupChatId(appGroupChat.getAppGroupChatId());
        if(appGroupChatMessagesOptional.isPresent()) {
            List<AppGroupChatMessage> appGroupChatMessages = new ArrayList<>();
            for(AppGroupChatMessage appGroupChatMessage : appGroupChatMessagesOptional.get()) {
                appGroupChatMessages.add(appGroupChatMessage);
            }
            return appGroupChatMessages;
        }
        return null;
    }

    @Transactional
    public AppGroup createAppGroup(AppGroup appGroup) {
        AppGroup newAppGroup = appGroupRepository.save(appGroup);
        AppGroupPlayer appGroupPlayer = new AppGroupPlayer();
        appGroupPlayer.setAppUserId(newAppGroup.getAppUserId());
        appGroupPlayer.setAppGroupId(newAppGroup.getAppGroupId());
        appGroupPlayerRepository.save(appGroupPlayer);
        AppGroupChat appGroupChat = new AppGroupChat();
        appGroupChat.setAppGroupId(newAppGroup.getAppGroupId());
        appGroupChatRepository.save(appGroupChat);
        return appGroupRepository.save(appGroup);
    }

    @Transactional
    public AppGroup updateAppGroup(AppGroup appGroup) {
        AppGroup appGroupToUpdate = appGroupRepository.getById(appGroup.getAppGroupId());
        if(DataValidationUtils.hasStringValueBeenChanged(appGroupToUpdate.getAppGroupName(), appGroup.getAppGroupName()))
            appGroupToUpdate.setAppGroupName(appGroup.getAppGroupName());
        if(DataValidationUtils.hasStringValueBeenChanged(appGroupToUpdate.getAppGroupDescription(), appGroup.getAppGroupDescription()))
            appGroupToUpdate.setAppGroupDescription(appGroup.getAppGroupDescription());
        return appGroupToUpdate;
    }

    @Transactional
    public void deleteAppGroup(long appGroupId) {
        AppGroup appGroup = appGroupRepository.getById(appGroupId);
        List<AppGroupPlayer> appGroupPlayers = appGroupPlayerRepository.findAllByAppGroupId(appGroupId).get();
        for(AppGroupPlayer appGroupPlayer: appGroupPlayers) {
            appGroupPlayerRepository.delete(appGroupPlayer);
        }
        AppGroupChat appGroupChat = appGroupChatRepository.findFirstByAppGroupId(appGroupId).get();
        List<AppGroupChatMessage> appGroupChatMessages = appGroupChatMessageRepository.findAllByAppGroupChatId(appGroupChat.getAppGroupChatId()).get();
        for(AppGroupChatMessage appGroupChatMessage: appGroupChatMessages) {
            appGroupChatMessageRepository.delete(appGroupChatMessage);
        }
        Optional<List<Announcement>> announcement = announcementRepository.findAllByGameIdAndAppUserIdAndAppGroupId(appGroup.getGameId(), appGroup.getAppUserId(), appGroup.getAppGroupId());
        if (announcement.isPresent()) {
            announcementRepository.delete(announcement.get().get(0));
        }
        appGroupChatRepository.delete(appGroupChat);
        appGroupRepository.delete(appGroupRepository.getById(appGroupId));
    }

    @Transactional
    public AppGroupPlayer addAppUserToAppGroup(long appUserId, long appGroupId) {
        AppGroupPlayer appGroupPlayer = new AppGroupPlayer();
        appGroupPlayer.setAppUserId(appUserId);
        appGroupPlayer.setAppGroupId(appGroupId);
        return appGroupPlayerRepository.save(appGroupPlayer);
    }

    @Transactional
    public void removeAppUserFromAppGroup(long appUserId, long appGroupId) {
        Optional<AppGroupPlayer> appGroupPlayer = appGroupPlayerRepository.findFirstByAppGroupIdAndAppUserId(appGroupId, appUserId);
        if(appGroupPlayer.isPresent())
            appGroupPlayerRepository.delete(appGroupPlayer.get());
    }

    @Transactional
    public AppGroupChatMessage addAppGroupChatMessageToAppGroup(AppGroupChatMessage appGroupChatMessage, long appGroupId) {
        AppGroupChat appGroupChat = appGroupChatRepository.findFirstByAppGroupId(appGroupId).get();
        AppGroupChatMessage appGroupChatMessageToSave = new AppGroupChatMessage();
        appGroupChatMessageToSave.setAppGroupChatId(appGroupChat.getAppGroupChatId());
        appGroupChatMessageToSave.setAppUserId(appGroupChatMessage.getAppUserId());
        appGroupChatMessageToSave.setAppGroupChatMessageMsg(appGroupChatMessage.getAppGroupChatMessageMsg());
        appGroupChatMessageToSave.setAppGroupChatMessageDate(new Date());
        return appGroupChatMessageRepository.save(appGroupChatMessageToSave);
    }

}

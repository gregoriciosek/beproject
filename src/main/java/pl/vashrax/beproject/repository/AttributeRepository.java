package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.Attribute;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {
    Optional<List<Attribute>> findAllByGameId(long gameId);
}


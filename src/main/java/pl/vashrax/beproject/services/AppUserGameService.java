package pl.vashrax.beproject.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.vashrax.beproject.entity.AppUserGame;
import pl.vashrax.beproject.repository.AppUserGameRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AppUserGameService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final AppUserGameRepository appUserGameRepository;

    private final AttributeService attributeService;

    @Transactional
    public AppUserGame addOrUpdateAttributeForGameAndAppUser(AppUserGame appUserGame){
        if(appUserGameRepository.existsAppUserGameByGameIdAndAppUserId(appUserGame.getGameId(), appUserGame.getAppUserId())) {
            AppUserGame appUserGameToUpdate = appUserGameRepository.findFirstAppUserGameByGameIdAndAppUserId(appUserGame.getGameId(), appUserGame.getAppUserId()).get();
            if(appUserGame.getAttributeId() != null && appUserGameToUpdate.getAttributeId() != appUserGame.getAttributeId()) {
                appUserGameToUpdate.setAttributeId(appUserGame.getAttributeId());
            }
            return appUserGameToUpdate;
        } else {
            return appUserGameRepository.save(appUserGame);
        }
    }

    public AppUserGame getAttributeForGameAndAppUser(AppUserGame appUserGame) {
        Optional<AppUserGame> appUserGameResult = appUserGameRepository.findFirstAppUserGameByGameIdAndAppUserId(appUserGame.getGameId(), appUserGame.getAppUserId());
        if (appUserGameResult.isPresent()) {
            return appUserGameResult.get();
        }
        return null;
    }

}

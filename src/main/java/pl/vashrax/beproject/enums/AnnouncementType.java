package pl.vashrax.beproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum AnnouncementType {
    LOOK_FOR_TEAM(1L,"LOOK_FOR_TEAM"),
    LOOK_FOR_PLAYERS(2L,"LOOK_FOR_PLAYERS");

    @Getter
    private Long id;
    @Getter
    private String symbol;

    public AnnouncementType parse(String symbol) {
        if (symbol.equals(LOOK_FOR_TEAM.getSymbol())) {
            return LOOK_FOR_TEAM;
        } else if (symbol.equals(LOOK_FOR_PLAYERS.getSymbol())) {
            return LOOK_FOR_PLAYERS;
        } else {
            return null;
        }
    }

    public static AnnouncementType parseId(Long id) {
        for (AnnouncementType type : AnnouncementType.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

}

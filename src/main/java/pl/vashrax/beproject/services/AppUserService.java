package pl.vashrax.beproject.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.w3c.dom.Attr;
import pl.vashrax.beproject.dto.AppUserDto;
import pl.vashrax.beproject.dto.AppUserFilterDto;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.entity.AppUserGame;
import pl.vashrax.beproject.entity.Attribute;
import pl.vashrax.beproject.mapper.AppUserMapper;
import pl.vashrax.beproject.repository.AppUserGameRepository;
import pl.vashrax.beproject.repository.AppUserRepository;
import pl.vashrax.beproject.repository.AttributeRepository;
import pl.vashrax.beproject.security.UserPrincipal;
import pl.vashrax.beproject.utils.DataValidationUtils;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AppUserService implements UserDetailsService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final AppUserRepository appUserRepository;
    private final AttributeRepository attributeRepository;
    private final AppUserGameRepository appUserGameRepository;

    @Override
    public UserDetails loadUserByUsername(String appUserUsername) throws UsernameNotFoundException {
        Optional<AppUser> user =  appUserRepository.findApplicationUserByAppUserUsername(appUserUsername);
        if(!user.isPresent())
            throw new UsernameNotFoundException(String.format("There is not user with username : %s", appUserUsername));
        else{
        UserPrincipal userPrincipal = new UserPrincipal(user.get());
        LOGGER.info("Returning found user by username : " + appUserUsername);
        return userPrincipal;
        }
    }

    public AppUser loadApplicationUserByUsername(String appUserUsername) throws UsernameNotFoundException {
        Optional<AppUser> user = appUserRepository.findApplicationUserByAppUserUsername(appUserUsername);
        if(!user.isPresent())
            throw new UsernameNotFoundException(String.format("There is not user with username : %s", appUserUsername));
        else{
            return user.get();
        }
    }

    @Transactional
    public AppUserDto registerUser(AppUser appUser){
        AppUser appUserTarget = addAndReturnNewUser(appUser);
        return AppUserMapper.mapToDto(appUserTarget);
    }

    @Transactional
    public AppUser addAndReturnNewUser(AppUser appUser) {
        Optional<AppUser> user = appUserRepository.findApplicationUserByAppUserUsername(appUser.getAppUserUsername());
        if(user.isPresent()){
            throw new IllegalStateException("Username : " + appUser.getAppUserUsername() + ", has been already taken.");
        }
        return appUserRepository.save(appUser);
    }

    public List<AppUser> findAllApplicationUsers(){
        return appUserRepository.findAll();
    }

    public AppUser findById(long id) {
        return appUserRepository.findById(id).orElseThrow(() -> new IllegalStateException("User with id " + id + " does not exist."));
    }

    @Transactional
    public void deleteById(Long id){
        Optional<AppUser> user = appUserRepository.findById(id);
        if(!user.isPresent()){
            throw new IllegalStateException("User with id " + id + " does not exist");
        }
        appUserRepository.deleteById(id);
    }

    @Transactional
    public AppUser updateAndReturnUser(AppUser appUser) {
        AppUser appUserToUpdate = appUserRepository.findApplicationUserByAppUserUsername(appUser.getAppUserUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + appUser.getAppUserUsername() + "does not exist."));
        if(DataValidationUtils.hasStringValueBeenChanged(appUserToUpdate.getAppUserNickname(), appUser.getAppUserNickname()))
            appUserToUpdate.setAppUserNickname(appUser.getAppUserNickname());
        if(DataValidationUtils.hasStringValueBeenChanged(appUserToUpdate.getAppUserPassword(), appUser.getAppUserPassword()))
            appUserToUpdate.setAppUserPassword(appUser.getAppUserPassword());
        if(DataValidationUtils.hasStringValueBeenChanged(appUserToUpdate.getAppUserEmail(), appUser.getAppUserEmail()))
            appUserToUpdate.setAppUserEmail(appUser.getAppUserEmail());
        if(DataValidationUtils.hasObjectValueBeenChanged(appUserToUpdate.getAppUserDateOfBirth(), appUser.getAppUserDateOfBirth()))
            appUserToUpdate.setAppUserDateOfBirth(appUser.getAppUserDateOfBirth());
        if(!Objects.equals(appUserToUpdate.getAppUserGender(), appUser.getAppUserGender()))
            appUserToUpdate.setAppUserGender(appUser.getAppUserGender());
        if(DataValidationUtils.hasStringValueBeenChanged(appUserToUpdate.getAppUserDiscord(), appUser.getAppUserDiscord()))
            appUserToUpdate.setAppUserDiscord(appUser.getAppUserDiscord());
        if(DataValidationUtils.hasStringValueBeenChanged(appUserToUpdate.getAppUserTeamspeak(), appUser.getAppUserTeamspeak()))
            appUserToUpdate.setAppUserTeamspeak(appUser.getAppUserTeamspeak());
        if(DataValidationUtils.hasStringValueBeenChanged(appUserToUpdate.getAppUserProfileImgUrl(), appUser.getAppUserProfileImgUrl()))
            appUserToUpdate.setAppUserProfileImgUrl(appUser.getAppUserProfileImgUrl());
        return appUserToUpdate;
    }

    public List<AppUser> getAppUsersByFilter(AppUserFilterDto appUserFilter) {
        List<AppUser> appUsers = appUserRepository.findAll();
        List<AppUser> appUsersTarget = new ArrayList<>();
        for(int i = 0; i<appUsers.size(); i++) {
            AppUser appUser = appUsers.get(i);
            //Sprawdzenie dat
            if(appUserFilter.getAgeFrom() != 0 || appUserFilter.getAgeTo() !=0) {
                if (appUser.getAppUserDateOfBirth() == null) {
                    continue;
                }
                if (appUserFilter.getAgeFrom() != 0) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.YEAR, - appUserFilter.getAgeFrom());
                    Date dateFrom = cal.getTime();
                    if (appUser.getAppUserDateOfBirth().before(cal.getTime())) {
                        continue;
                    }
                }
                if (appUserFilter.getAgeTo() !=0) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.YEAR, - appUserFilter.getAgeTo());
                    if (appUser.getAppUserDateOfBirth().after(cal.getTime())) {
                        continue;
                    }
                }
            }
            //Sprawdzenie rangi
            if (appUserFilter.getGameAttribute() != 0) {
                Attribute attribute = attributeRepository.findById(appUserFilter.getGameAttribute()).get();
                Optional<AppUserGame> appUserGame = appUserGameRepository.findFirstAppUserGameByGameIdAndAppUserId(attribute.getGameId(), appUser.getAppUserId());
                if (appUserGame.isPresent()) {
                    Attribute appUserAttribute = attributeRepository.getById(appUserGame.get().getAttributeId());
                    if (appUserAttribute.getAttributeValue() < attribute.getAttributeValue()) {
                        continue;
                    }
                } else {
                    continue;
                }
            }
            //Sprawdzneie
            if (appUserFilter.getGender() != null && !appUserFilter.getGender().equals(appUser.getAppUserGender())) {
                if (appUser.getAppUserGender().equals("NONE")) {
                    continue;
                }
            }
            appUsersTarget.add(appUser);
        }
        return appUsersTarget;
    }
}

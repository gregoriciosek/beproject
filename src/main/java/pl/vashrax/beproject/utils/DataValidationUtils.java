package pl.vashrax.beproject.utils;

import java.util.Objects;

public class DataValidationUtils {

    public static boolean hasStringValueBeenChanged(String oldValue, String newValue) {
        return newValue !=null && newValue.length()>0 && !Objects.equals(oldValue, newValue);
    }

    public static boolean hasObjectValueBeenChanged(Object oldValue, Object newValue) {
        return newValue !=null && !Objects.equals(oldValue, newValue);
    }

    public static boolean hasNonZeroIntValueBeenChanged(int oldValue, int newValue) {
        return newValue!=0 && !Objects.equals(oldValue, newValue);
    }

    public static boolean hasValueBeenChanged(int oldValue, int newValue) {
        return newValue!=0 && !Objects.equals(oldValue, newValue);
    }

}

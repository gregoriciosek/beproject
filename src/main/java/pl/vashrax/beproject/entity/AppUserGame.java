package pl.vashrax.beproject.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "app_user_game")
public class AppUserGame implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long appUserGameId;
    private Long appUserId;
    private Long gameId;
    private Long attributeId;

}

package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.vashrax.beproject.dto.AppUserDto;
import pl.vashrax.beproject.enums.AppUserRole;
import pl.vashrax.beproject.mapper.AppUserMapper;
import pl.vashrax.beproject.services.AppUserService;
import pl.vashrax.beproject.entity.AppUser;

import static org.springframework.http.HttpStatus.*;

@Controller
@AllArgsConstructor
public class RegisterController {

    private final AppUserService appUserService;

    private final PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    public ResponseEntity<AppUserDto> registerNewUser(@RequestBody AppUserDto appUserDto) {
        AppUser appUser = AppUserMapper.mapToEntity(appUserDto);
        appUser.setAppUserPassword(passwordEncoder.encode(appUser.getAppUserPassword()));
        appUser.setAppUserTypeId(AppUserRole.USER.getId());
        return new ResponseEntity<>(appUserService.registerUser(appUser), OK);
    }
}


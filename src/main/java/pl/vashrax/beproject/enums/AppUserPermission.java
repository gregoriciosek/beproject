package pl.vashrax.beproject.enums;

import lombok.Getter;

public enum AppUserPermission {
    USER_AUTH("user:role"),
    ADMIN_AUTH("admin:role");


    @Getter
    private final String permission;

    AppUserPermission(String permission) {
        this.permission = permission;
    }


}

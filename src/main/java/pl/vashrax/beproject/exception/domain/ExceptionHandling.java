package pl.vashrax.beproject.exception.domain;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.vashrax.beproject.config.HttpResponse;

import java.util.Objects;

import static pl.vashrax.beproject.enums.ExceptionMessage.*;

@Slf4j
@RestControllerAdvice
public class ExceptionHandling {

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    @ExceptionHandler({CredencialsIncorrectException.class})
    public ResponseEntity<HttpResponse> credencialsIncorrectException() {
        return createHttpResponse(HttpStatus.BAD_REQUEST, CREDEMCIALS_INCORRECT_EXCEPTION.getMessage());
    }

    @ExceptionHandler({UsernameNotFoundException.class})
    public ResponseEntity<HttpResponse> usernameNotFoundException() {
        return createHttpResponse(HttpStatus.NOT_FOUND, USERNAME_NOT_FOUND_EXCEPTION.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<HttpResponse> httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        HttpMethod supportedMethod = Objects.requireNonNull(exception.getSupportedHttpMethods()).iterator().next();
        return createHttpResponse(HttpStatus.METHOD_NOT_ALLOWED, String.format(HTTP_METHOD_NOT_SUPPORTED_EXCEPTION.getMessage(), supportedMethod));
    }

    private ResponseEntity<HttpResponse> createHttpResponse(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus,
                httpStatus.getReasonPhrase().toUpperCase(), message), httpStatus);
    }

}

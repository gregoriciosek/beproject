package pl.vashrax.beproject.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.vashrax.beproject.entity.*;
import pl.vashrax.beproject.enums.AppUserRole;
import pl.vashrax.beproject.repository.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import static pl.vashrax.beproject.enums.AppUserRole.*;

@Configuration
public class PreOriginConfig {

    private final PasswordEncoder passwordEncoder;
    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    public PreOriginConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    CommandLineRunner commandLineRunner2(AppUserRepository appUserRepository, ChatMessageRepository messageRepository, ChatRepository chatRepository){
        return args -> {

//            AppUser appUser = new AppUser();
//            appUser.setAppUserTypeId(USER.getId());
//            appUser.setAppUserUsername("ciocho5");
//            appUser.setAppUserPassword(passwordEncoder.encode("6825880"));
//            appUser.setAppUserEmail("ciocho5@gmail.com");
//            appUser.setAppUserNickname("Izianax");
//            appUser.setAppUserDateOfBirth(getDate(1992, 10, 12));
//            appUserRepository.save(appUser);
//            LOGGER.info("Dodano użytkownika appUser");
//
//            AppUser appUser2 = new AppUser();
//            appUser2.setAppUserTypeId(USER.getId());
//            appUser2.setAppUserUsername("ciocho6");
//            appUser2.setAppUserPassword(passwordEncoder.encode("ssxbi5ms"));
//            appUser2.setAppUserEmail("gregoriciosek@gmail.com");
//            appUser2.setAppUserNickname("Vashrax");
//            appUser2.setAppUserDateOfBirth(getDate(1991, 11, 11));
//            appUserRepository.save(appUser2);
//            LOGGER.info("Dodano użytkownika appUser2");

//            AppUser a1 = new AppUser("admin",
//                    "admin",
//                    "admin@admin.com",
//                    passwordEncoder.encode("admin"),
//                    ADMIN,
//                    true,
//                    true);
//            a1.setProfileImgUrl("https://bootdey.com/img/Content/avatar/avatar"+ getRandomInt() +".png");
//            a1.setSteam("http://steamcommunity.com/profiles/76561197965277786");
//            a1.setOrigin("VashRaX");
//            a1.setBattleNet("VashRaX");
//            a1.setEpicGames("VashRaX");
//            a1.setUbisoftConnect("VashRaX");
//            a1.setWannaPlay(true);
//            a1.setUsingMicrophone(true);
//            a1.setYear_of_birth(29);
//            a1.setDescription("Jestem początkującym graczem, posiadam mikrofon i staram się go używać.");
//            AppUser a2 = new AppUser("admintr",
//                    "admintr",
//                    "admintr@admin.com",
//                    passwordEncoder.encode("admintr"),
//                    ADMINTRAINEE,
//                    true,
//                    true);
//            a2.setProfileImgUrl("https://bootdey.com/img/Content/avatar/avatar"+ getRandomInt() +".png");
//            AppUser a3 = new AppUser("student",
//                    "student",
//                    "student@admin.com",
//                    passwordEncoder.encode("student"),
//                    USER,
//                    true,
//                    true);
//            a3.setProfileImgUrl("https://bootdey.com/img/Content/avatar/avatar"+ getRandomInt() +".png");
//            appUserRepository.save(a1);
//            LOGGER.info("Dodano użytkownika a1");
//            appUserRepository.save(a2);
//            LOGGER.info("Dodano użytkownika a2");
//            appUserRepository.save(a3);
//            LOGGER.info("Dodano użytkownika a3");
//
//            for (int iterator = 0; iterator <= 100; iterator++){
//                String name = generateName();
//                AppUser a = new AppUser(name,
//                        "X"+name+"X",
//                        name + "@gmail.com",
//                        passwordEncoder.encode(name),
//                        USER,
//                        true,
//                        true);
//                a.setProfileImgUrl("https://bootdey.com/img/Content/avatar/avatar"+ getRandomInt() +".png");
//                        appUserRepository.save(a);
//
//            }
        };
    }
    private String generateName(){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }

    private int getRandomInt() {
        int min = 1;
        int max = 8;
        return (int) (Math.floor(Math.random() * (max - min)) + min);
    }

    private Date getDate(int year, int month, int day) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

}

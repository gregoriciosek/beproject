package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.AppGroupChat;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Repository
public interface AppGroupChatRepository extends JpaRepository<AppGroupChat, Long> {
    Optional<AppGroupChat> findFirstByAppGroupId(long appGroupId);

}


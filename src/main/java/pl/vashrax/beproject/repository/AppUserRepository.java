package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.AppUser;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    @Query
    Optional<AppUser> findApplicationUserByAppUserUsername(String appUserUsername);

}

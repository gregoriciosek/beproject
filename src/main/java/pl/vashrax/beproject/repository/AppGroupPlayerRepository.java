package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.AppGroupPlayer;
import pl.vashrax.beproject.entity.AppUserGame;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface AppGroupPlayerRepository extends JpaRepository<AppGroupPlayer, Long> {

    Optional<List<AppGroupPlayer>> findAllByAppUserId(long appUserId);

    Optional<List<AppGroupPlayer>> findAllByAppGroupId(long appGroupId);

    Optional<AppGroupPlayer> findFirstByAppGroupIdAndAppUserId(long appGroupId, long appUserId);

}

package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.Game;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

}

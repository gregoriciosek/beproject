package pl.vashrax.beproject.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "app_group_chat")
public class AppGroupChat implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long appGroupChatId;
    private Long appGroupId;
}

package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.AppUserGame;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Repository
public interface AppUserGameRepository extends JpaRepository<AppUserGame, Long> {

    boolean existsAppUserGameByGameIdAndAppUserId(long gameId, long appUserId);
    Optional<AppUserGame> findFirstAppUserGameByGameIdAndAppUserId(long gameId, long appUserId);

}


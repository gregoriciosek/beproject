package pl.vashrax.beproject.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.vashrax.beproject.entity.AppGroupPlayer;
import pl.vashrax.beproject.repository.AnnouncementRepository;
import pl.vashrax.beproject.entity.Announcement;
import pl.vashrax.beproject.repository.AppGroupPlayerRepository;
import pl.vashrax.beproject.utils.DataValidationUtils;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AnnouncementService {

    private final AnnouncementRepository announcementRepository;

    private final AppGroupPlayerRepository appGroupPlayerRepository;

    public Announcement getAnnouncementByAnnouncementId(long announcementId) {
        return announcementRepository.getById(announcementId);
    }

    public List<Announcement> getAnnouncementsForGameId(long gameId) {
        Optional<List<Announcement>> announcementsOptional = announcementRepository.findAllByGameId(gameId);
        if (announcementsOptional.isPresent()) {
            return announcementsOptional.get();
        }
        return null;
    }

    @Transactional
    public Announcement createAnnouncement(Announcement announcement) {
        Announcement announcementToCreate = new Announcement();
        announcementToCreate.setGameId(announcement.getGameId());
        announcementToCreate.setAppUserId(announcement.getAppUserId());
        announcementToCreate.setAnnouncementTitle(announcement.getAnnouncementTitle());
        announcementToCreate.setAnnouncementDescription(announcement.getAnnouncementDescription());
        announcementToCreate.setAnnouncementCreatedTime(new Date());
        announcementToCreate.setAppGroupId(announcement.getAppGroupId());
        return announcementRepository.save(announcementToCreate);
    }

    @Transactional
    public Announcement updateAnnouncement(Announcement announcement) {
        Announcement announcementToUpdate = announcementRepository.getById(announcement.getAnnouncementId());
        if(DataValidationUtils.hasStringValueBeenChanged(announcementToUpdate.getAnnouncementTitle(),announcement.getAnnouncementTitle()))
            announcementToUpdate.setAnnouncementTitle(announcement.getAnnouncementTitle());
        if(DataValidationUtils.hasStringValueBeenChanged(announcementToUpdate.getAnnouncementDescription(), announcement.getAnnouncementDescription()))
            announcementToUpdate.setAnnouncementDescription(announcement.getAnnouncementDescription());
        return announcementToUpdate;
    }

    @Transactional
    public void deleteAnnouncement(Announcement announcement) {
        announcementRepository.delete(announcement);
    }

    @Transactional
    public AppGroupPlayer addAppUserToGroupByAnnouncement(AppGroupPlayer appGroupPlayer) {
        List<AppGroupPlayer> playersInGroup = appGroupPlayerRepository.findAllByAppGroupId(appGroupPlayer.getAppGroupId()).get();
        if(playersInGroup.size()<5) {
            AppGroupPlayer appGroupPlayerToSave = new AppGroupPlayer();
            appGroupPlayerToSave.setAppGroupId(appGroupPlayer.getAppGroupId());
            appGroupPlayerToSave.setAppUserId(appGroupPlayer.getAppUserId());
            return appGroupPlayerRepository.save(appGroupPlayerToSave);
        } else {
            throw new IllegalStateException("Grupa zaiwera maksymanlą ilośc użytkowników - 5.");
        }
    }

}

package pl.vashrax.beproject.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "chat_message")
public class ChatMessage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long chatMessageId;
    private Long appUserId;
    private Long chatId;
    private String chatMessageMsg;
    @CreatedDate
    private Date chatMessageDate;

}

package pl.vashrax.beproject.dto;

import lombok.Data;

@Data
public class ChatUsersAndChatMessageDto {
    private Long appUserId;
    private Long appAppUserId;
    private String chatMessageMsg;
}

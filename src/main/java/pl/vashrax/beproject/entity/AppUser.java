package pl.vashrax.beproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "app_user")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AppUser implements Serializable, IAppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long appUserId;
    private Long appUserTypeId;

    private String appUserUsername;
    private String appUserPassword;
    private String appUserEmail;
    private String appUserNickname;
    private Date appUserDateOfBirth;

    @CreatedDate
    private Date createdAt;
    private String appUserDiscord;
    private String appUserTeamspeak;
    private String appUserGender;
    private String appUserProfileImgUrl;
}

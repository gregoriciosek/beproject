package pl.vashrax.beproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.vashrax.beproject.entity.Announcement;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {

    Optional<List<Announcement>> findAllByGameId(long gameId);

    Optional<List<Announcement>> findAllByGameIdAndAppUserIdAndAppGroupId(long gameId, long appUserId, long appGroupId);

}


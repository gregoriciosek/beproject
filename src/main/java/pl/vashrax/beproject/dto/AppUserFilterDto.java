package pl.vashrax.beproject.dto;

import lombok.Data;

@Data
public class AppUserFilterDto {
    long gameAttribute;
    int ageFrom;
    int ageTo;
    String gender;
}

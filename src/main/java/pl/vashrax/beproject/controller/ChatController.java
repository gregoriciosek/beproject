package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.dto.ChatUsersAndChatMessageDto;
import pl.vashrax.beproject.entity.AppUser;
import pl.vashrax.beproject.entity.Chat;
import pl.vashrax.beproject.entity.ChatMessage;
import pl.vashrax.beproject.services.ChatService;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
@RequestMapping("/chats")
public class ChatController {

    @Autowired
    private ChatService chatService;

    @GetMapping("/all")
    public ResponseEntity<List<Chat>> getAllChats(){
        List<Chat> chats = chatService.findAllChats();
        return new ResponseEntity<>(chats, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Chat> getChatById(@PathVariable("id") long id){
        Chat chat = chatService.findChatbyId(id);
        return new ResponseEntity<>(chat, HttpStatus.OK);
    }
    @PostMapping("")
    public ResponseEntity<Chat> addOrUpdateChatWithMessage(
            @RequestBody ChatUsersAndChatMessageDto chatUsersAndChatMessageDto) {
        Chat chat = chatService.findChatByAppUserIds(new long[]{chatUsersAndChatMessageDto.getAppUserId(),
                chatUsersAndChatMessageDto.getAppAppUserId()});
        if (chat == null) {
            chat.setAppUserId(chatUsersAndChatMessageDto.getAppUserId());
            chat.setAppAppUserId(chatUsersAndChatMessageDto.getAppAppUserId());
            chat = chatService.createChat(chat);
        }
        chatService.createMessageForSenderIdAndChatId(chatUsersAndChatMessageDto.getAppUserId(),
                chat.getChatId(), chatUsersAndChatMessageDto.getChatMessageMsg());
        return new ResponseEntity<>(chat, OK);
    }
    @GetMapping("/{chatId}/messages")
    public ResponseEntity<List<ChatMessage>> getMessagesByChatId(@PathVariable("chatId") long chatId) {
        List<ChatMessage> messages = null;
        if( chatService.existsByChatId(chatId)) {
            messages = chatService.getChatMessagesByChatId(chatId);
        }
        return new ResponseEntity<>(messages, OK);
    }

    @PostMapping("/users/messages")
    public ResponseEntity<List<ChatMessage>> getMessagesByAppUserIds(
            @RequestBody ChatUsersAndChatMessageDto chatUsersAndChatMessageDto) {
        List<ChatMessage> messages = null;
        Chat chat = chatService.findChatByAppUserIds(new long[]{chatUsersAndChatMessageDto.getAppUserId(),
                chatUsersAndChatMessageDto.getAppAppUserId()});
        if( chatService.existsByChatId(chat.getChatId())) {
            messages = chatService.getChatMessagesByChatId(chat.getChatId());
        }
        return new ResponseEntity<>(messages, OK);
    }

    @GetMapping("/users/{appUserId}")
    public List<AppUser> findAppUsersByChatsByAppUserId(@PathVariable("appUserId") long appUserId) {
        return chatService.findAppUsersByChatsByAppUserId(appUserId);
    }
}

package pl.vashrax.beproject.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "app_group_chat_message")
public class AppGroupChatMessage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long appGroupChatMessageId;
    private Long appGroupChatId;
    private Long appUserId;
    private String appGroupChatMessageMsg;
    private Date appGroupChatMessageDate;

}
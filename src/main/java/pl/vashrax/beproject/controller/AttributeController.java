package pl.vashrax.beproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.vashrax.beproject.entity.AppUserGame;
import pl.vashrax.beproject.entity.Attribute;
import pl.vashrax.beproject.services.AttributeService;

import static org.springframework.http.HttpStatus.OK;

@Controller
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
@RequestMapping("/attributes")
public class AttributeController {

    @Autowired
    private final AttributeService attributeService;

    @GetMapping("/{attributeId}")
    public ResponseEntity<Attribute> getAttributeByAttributeId(@PathVariable("attributeId") long attributeId){
        Attribute attribute = attributeService.getAttributeByAttributeId(attributeId);
        return new ResponseEntity<>(attribute, HttpStatus.OK);
    }

    @PostMapping("/get")
    public ResponseEntity<Attribute> getAttributeNameForGameAndAppUser(@RequestBody AppUserGame appUserGame) {
        Attribute attributeName = attributeService.getAttributeNameForGameAndAppUser(appUserGame);
        return new ResponseEntity<>(attributeName, OK);
    }

}

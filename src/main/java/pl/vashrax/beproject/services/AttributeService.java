package pl.vashrax.beproject.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.vashrax.beproject.entity.AppUserGame;
import pl.vashrax.beproject.entity.Attribute;
import pl.vashrax.beproject.repository.AppUserGameRepository;
import pl.vashrax.beproject.repository.AttributeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AttributeService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final AttributeRepository attributeRepository;

    private final AppUserGameRepository appUserGameRepository;

    public List<Attribute> getAttributesByGameId(long gameId) {
        List<Attribute> attributes = attributeRepository.findAllByGameId(gameId).get();
        return attributes;
    }

    public Attribute getAttributeByAttributeId(long attributeId) {
        Attribute attribute = attributeRepository.findById(attributeId).get();
        return attribute;
    }

    public Attribute getAttributeNameForGameAndAppUser(AppUserGame appUserGame) {
        Optional<AppUserGame> appUserGameOptional = appUserGameRepository.findFirstAppUserGameByGameIdAndAppUserId(appUserGame.getGameId(), appUserGame.getAppUserId());
        if (appUserGameOptional.isPresent()) {
            return attributeRepository.findById(appUserGameOptional.get().getAttributeId()).get();
        }
        return null;
    }

}

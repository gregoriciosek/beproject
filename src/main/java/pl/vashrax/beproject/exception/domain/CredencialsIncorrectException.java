package pl.vashrax.beproject.exception.domain;

public class CredencialsIncorrectException extends Exception{

    public CredencialsIncorrectException() {
        super();
    }

    public CredencialsIncorrectException(String message) {
        super(message);
    }

    public CredencialsIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }
}

package pl.vashrax.beproject.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.vashrax.beproject.entity.Game;
import pl.vashrax.beproject.repository.GameRepository;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class GameService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final GameRepository gameRepository;

    public List<Game> findAllGames(){
        return gameRepository.findAll();
    }

}

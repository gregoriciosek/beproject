package pl.vashrax.beproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "announcement")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Announcement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long announcementId;
    private Long appUserId;
    private Long gameId;
    private String announcementTitle;
    private String announcementDescription;
    @CreatedDate
    private Date announcementCreatedTime;
    private Long appGroupId;

}
